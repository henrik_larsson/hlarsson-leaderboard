const ExtractTextPlugin = require("extract-text-webpack-plugin")

module.exports = {
  devtool: 'source-map',

  entry: './',

  output: {
    path: __dirname + '/__build__',
    filename: 'build.js',
    publicPath: '__build__'
  },

  resolve: {
    extensions: [ '.js', '.css' ]
  },

  module: {
    loaders: [
      {
        test: /\.css$/,
        use: ExtractTextPlugin.extract({
          fallback: 'style-loader',
          use: 'css-loader?modules&importLoaders=1&localIdentName=[name]__[local]___[hash:base64:5]/!postcss-loader'
        })
      },
      { test: /\.js$/, exclude: /node_modules|mocha-browser\.js/, loader: 'babel-loader' },
      { test: /\.woff(2)?(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'url-loader?limit=10000&mimetype=application/font-woff' },
      { test: /\.(ttf|eot|svg)(\?v=[0-9]\.[0-9]\.[0-9])?$/, loader: 'file-loader' }
    ]
  },

  plugins: [
    new ExtractTextPlugin('main.css')
  ],

  // plugins: [
  //     new webpack.optimize.CommonsChunkPlugin({ name: 'shared' })
  // ],

  devServer: {
    quiet: false,
    noInfo: false,
    historyApiFallback: {
      rewrites: [
        {
          from: /ReduxDataFlow\/exercise.html/,
          to: 'ReduxDataFlow\/exercise.html'
        }
      ]
    },
    stats: {
      // Config for minimal console.log mess.
      assets: true,
      colors: true,
      version: true,
      hash: true,
      timings: true,
      chunks: false,
      chunkModules: false
    }
  }
}
