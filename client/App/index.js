import React from 'react'

import Round from './components/Round'
import Header from './components/Header'
import MainMenu from './components/MainMenu'

import styles from './styles.css'

const mainMenu = [
  { href: 'history', icon: 'history', label: 'History' },
  { href: 'stats', icon: 'list', label: 'Stats' },
  { href: 'new', icon: 'play-circle', label: 'New Round' },
]

const players = [
  { name: 'Henrik', currentScore: 1, id: 1 },
  { name: 'Christian', currentScore: -2, id: 2}
]

const App = () => {
  return (
    <div className={styles.mainLayout}>
      <Header />
      <Round players={players} currentHole={3} />
      <MainMenu menuItems={mainMenu} />
    </div>
  )
}

export default App
