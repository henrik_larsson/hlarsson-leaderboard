import React from 'react'

import styles from './styles.css'

const Button = ({ icon }) => (
  <div className={styles.button}>
    <i className={`fa fa-${icon}`} />
  </div>
)

export default Button
