import React from 'react'

import styles from './styles.css'

const Header = () => {
  return (
    <div className={styles.header}>LeaderBoard</div>
  )
}

export default Header
