import React from 'react'
import PropTypes from 'prop-types'

import styles from './styles.css'

const MenuItem = ({ href, icon, label }) => {
  return (
    <div className={styles.menuItem}>
      <i className={`fa fa-${icon} fa-3x`}></i>{label}
    </div>
  )
}

MenuItem.propTypes = {
  href: PropTypes.string.isRequired,
  icon: PropTypes.string.isRequired,
  label: PropTypes.string.isRequired
}

export default MenuItem
