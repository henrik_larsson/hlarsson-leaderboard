import React from 'react'
import PropTypes from 'prop-types'

import MenuItem from './MenuItem'

import styles from './styles.css'

const Menu = ({ menuItems }) => {
  const menuItemElements = menuItems.map((menuItem) => (
    <li key={menuItem.href}>
      <MenuItem href={menuItem.href} icon={menuItem.icon} label={menuItem.label} />
    </li>))

  return (
    <ul className={styles.mainMenu}>
      {menuItemElements}
    </ul>
  )
}

Menu.propTypes = {
  menuItems: PropTypes.arrayOf(
    PropTypes.shape(MenuItem.propTypes)
  ).isRequired
}

export default Menu
