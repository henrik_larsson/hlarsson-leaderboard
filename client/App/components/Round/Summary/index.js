import React from 'react'

import styles from './styles.css'

const formatScore = score => score > 0 ? `+${score}` : score

const Summary = ({ players }) => {
  const currentScoreElements = players.map(player => (
    <div key={player.id} className={styles.currentScore}>
      <div>{player.name}</div>
      <div>{formatScore(player.currentScore)}</div>
    </div>
  ));
  return (
    <div className={styles.summaryContainer}>
      {currentScoreElements}
    </div>
  )
}

export default Summary
