import React from 'react'

import Button from '../../Button'

import styles from './styles.css'

const Score = ({ player }) => (
  <div className={styles.scoreContainer}>
    <div className={styles.name}>{player.name}</div>
    <Button icon="minus-circle" className={styles.minus}/>
    <div className={styles.score}>3</div>
    <Button icon="plus-circle" className={styles.plus}/>
  </div>
)

export default Score
