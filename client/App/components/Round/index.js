import React from 'react'

import Button from '../Button'
import Score from './Score'
import Summary from './Summary'

import styles from './styles.css'

const Round = ({ players, currentHole }) => {
  const playerElements = players.map(player => (
    <Score key={player.id} player={player} />
  ))
  return (
    <div className={styles.roundContainer}>
      <Summary players={players} />
      <h1 className={styles.currentHole}>Hole {currentHole}</h1>
      {playerElements}
    </div>
  )
}

export default Round
