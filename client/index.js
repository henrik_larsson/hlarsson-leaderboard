import React from 'react'
import { render } from 'react-dom'
import App from './App'
import fa from 'font-awesome-webpack'

render(
  <App />,
  document.getElementById('app')
)
